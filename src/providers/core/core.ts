import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the CoreProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CoreProvider {

  private count:any = 0;

  constructor(public event: Events, public httpClient: HttpClient) {
    console.log('CoreProvider Init');

    // Test Notification to be sent
    setTimeout(this.incUnreadCount.bind(this), 1000);
    setTimeout(this.incUnreadCount.bind(this), 5000);
  }

  incUnreadCount () {
    this.count++;
    this.event.publish('notification-update', {count: this.count});
  }

  getCount () {
    let _root = this;
    return new Promise(function(resolve, reject) {
      resolve(_root.count);
    });
  }

}
