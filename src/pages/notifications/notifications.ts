import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private event:Events) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  doRefresh(refresher) {
    console.log('Begin async operation');
    this.event.publish('syncing');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
      this.event.publish('sync-end');
    }, 1000);
  }

}
