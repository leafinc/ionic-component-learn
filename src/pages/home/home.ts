import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { TabWrapperPage } from '../tab-wrapper/tab-wrapper';
import { HelpPage } from '../help/help';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private pages:Object = [{
    id: 'i1',
    title: 'Complaints',
    page: TabWrapperPage,
    isRoot: false,
    complaintIcon: "/assets/imgs/complaint.png"
  },{
    id: 'i2',
    title: 'Notices',
    page: null,
    isRoot: false,
    complaintIcon: "/assets/imgs/notice.png"
  },{
    id: 'i3',
    title: 'Help',
    page: HelpPage,
    isRoot: false,
    complaintIcon: "/assets/imgs/help.png"
  }];

  constructor(public navCtrl: NavController, private alertCtrl:AlertController) {
    let _root = this;
    setTimeout(function() {
      _root.setStyle(_root.pages[0]);
      _root.setStyle(_root.pages[1]);
      _root.setStyle(_root.pages[2]);
    }, 10);
  }

  openPage(p) {
    if (p.page != null) {
      if (p.isRoot) {
        this.navCtrl.setRoot(p.page);
      } else {
      this.navCtrl.push(p.page);
      }
    } else {
      this.alertCtrl.create({
        title: 'Not Available',
        buttons: ['OK'],
        message: 'This function is not available in Beta&trade; Version'
      }).present();
    }
  }

  setStyle(obj) {
    var style = document.querySelector('style') || document.createElement('style');
    
    style.innerText = style.innerText + '#' + obj.id + '::before {background-image: url("'+obj.complaintIcon+'");}';
    if (document.querySelector('style') == null) {
      document.body.appendChild(style);
    }
  }

}
