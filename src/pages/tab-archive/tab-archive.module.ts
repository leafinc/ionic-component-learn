import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabArchivePage } from './tab-archive';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TabArchivePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TabArchivePage),
  ],
})
export class TabArchivePageModule {}
