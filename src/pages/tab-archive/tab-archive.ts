import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the TabArchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-archive',
  templateUrl: 'tab-archive.html',
})
export class TabArchivePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private event:Events) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabArchivePage');
  }

  doRefresh(refresher) {
    console.log('Begin async operation');
    this.event.publish('syncing');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
      this.event.publish('sync-end');
    }, 1000);
  }
}
