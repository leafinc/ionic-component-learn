import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabWrapperPage } from './tab-wrapper';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TabWrapperPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TabWrapperPage),
  ],
})
export class TabWrapperPageModule {}
