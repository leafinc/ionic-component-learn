import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabActivePage } from '../tab-active/tab-active';
import { TabArchivePage } from '../tab-archive/tab-archive';

/**
 * Generated class for the TabWrapperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-wrapper',
  templateUrl: 'tab-wrapper.html',
})
export class TabWrapperPage {

  private title:String;
  private tab1Root:any = TabActivePage;
  private tab2Root:any = TabArchivePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title = "All Complaints";
  }

  onTab(title:String) {
    this.title = title;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabWrapperPage');
  }

}
