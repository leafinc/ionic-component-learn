import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the TabActivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-active',
  templateUrl: 'tab-active.html',
})
export class TabActivePage {

  private icons:Array<Object> = [{
    id: 'ci1',
    complaintIcon: '/assets/imgs/water.png'
  },{
    id: 'ci2',
    complaintIcon: '/assets/imgs/electricity.png'
  },{
    id: 'ci3',
    complaintIcon: '/assets/imgs/park.png'
  }];

  constructor(public navCtrl: NavController, public navParams: NavParams, private event:Events) {
    this.icons.forEach(this.setStyle);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabActivePage');
  }

  setStyle(obj) {
    var style = document.querySelector('style') || document.createElement('style');
    
    style.innerText = style.innerText + '.' + obj.id + '::before {background-image: url("'+obj.complaintIcon+'");}';
    if (document.querySelector('style') == null) {
      document.body.appendChild(style);
    }
  }

  doRefresh(refresher) {
    console.log('Begin async operation');
    this.event.publish('syncing');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
      this.event.publish('sync-end');
    }, 1000);
  }

}
