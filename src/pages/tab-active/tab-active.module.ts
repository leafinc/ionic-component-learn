import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabActivePage } from './tab-active';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TabActivePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TabActivePage),
  ],
})
export class TabActivePageModule {}
