import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClient, HttpClientModule } from '@angular/common/http';
/** Providers */
import { CoreProvider } from '../providers/core/core';
/** ComponentsModule */
import { ComponentsModule } from '../components/components.module';
import { HelpPageModule } from '../pages/help/help.module';
import { NotificationsPageModule } from '../pages/notifications/notifications.module';
import { TabWrapperPageModule } from '../pages/tab-wrapper/tab-wrapper.module';
import { TabActivePageModule } from '../pages/tab-active/tab-active.module';
import { TabArchivePageModule } from '../pages/tab-archive/tab-archive.module';
/** App Component */
import { MyApp } from './app.component';
/** Pages */
import { HomePage } from '../pages/home/home';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HelpPageModule,
    NotificationsPageModule,
    TabWrapperPageModule,
    TabActivePageModule,
    TabArchivePageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CoreProvider
  ]
})
export class AppModule {}
