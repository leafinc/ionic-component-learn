import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { HelpPage } from '../pages/help/help';
import { TabWrapperPage } from '../pages/tab-wrapper/tab-wrapper';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav:Nav;

  rootPage:any = HomePage;
  pages:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.pages = [
      {title:'Home', page: HomePage, isRoot: true},
      {title:'Help', page: HelpPage, isRoot: false},
      {title:'Complaints', page: TabWrapperPage, isRoot: false}
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openPage(p) {
    if (p.isRoot) {
      this.nav.setRoot(p.page);
    } else {
      this.nav.push(p.page);
    }
  }
}

