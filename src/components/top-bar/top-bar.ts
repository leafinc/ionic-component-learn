import { Component, Input } from '@angular/core';
import { Events, App } from 'ionic-angular';
import { CoreProvider } from '../../providers/core/core';
import { NotificationsPage } from '../../pages/notifications/notifications';

/**
 * Generated class for the TopBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'top-bar',
  templateUrl: 'top-bar.html'
})
export class TopBarComponent {
  @Input() title: string;
  @Input() notify: string = 'true';

  private refreshing:Boolean;
  private unreadNotifications:Number = 0;

  constructor(private app: App,private event:Events, private core:CoreProvider) {
    this.refreshing = false;
    this.event.subscribe('syncing', (e) => {
      this.refreshing = true;
    });
    this.event.subscribe('sync-end', (e) => {
      this.refreshing = false;
    });
    this.event.subscribe('notification-update', (e) => {
      console.log('notification-update');
      this.unreadNotifications = e.count;
    });

    // Update the Count
    this.core.getCount().then((count:Number)=>this.unreadNotifications=count);
  }

  toShow() {
    let notify = (this.notify == 'true');
    return (notify && !this.refreshing);
  }

  openNotifications() {
    this.app.getRootNavs()[0].push(NotificationsPage);
  }

  hasUnread() {
    return !(Boolean)(this.unreadNotifications);
  }
}
