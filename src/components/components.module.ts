import { NgModule } from '@angular/core';
import { TopBarComponent } from './top-bar/top-bar';
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [TopBarComponent],
	imports: [IonicModule.forRoot(TopBarComponent)],
	exports: [TopBarComponent]
})
export class ComponentsModule {}
